#!/usr/bin/env php

<?php
/**
 * Restart Task
 * Created by Tenseg LLC, dev@tenseg.net
 * November 2021
 *
 * This script takes checks the uptime log, if it exists, and measures how long since the last log.
 * If it has been over 10 seconds it restarts the Local site.
 *
 * Run this in a VS Code Task to automate the process in your IDE.
 */

// include our config file
// must be in plugin so can be used by external task script
include 'config.php';

// get and check settings
$log = constant( 'TG_LOCAL_RESTARTER_LOG' );
$lbf = constant( 'TG_LOCAL_RESTARTER_LBF_PATH' );
if ( '' != $log && '' != $lbf ) {
	while ( 1 ) {
		// check the timestamp in the log
		check_log_timestamp();
		// sleep
		sleep( 10 );
	}
} else {
	echo 'Please add your own settings into your config file.';
}

/**
 * Check the uptime log.
 *
 * @return void
 */
function check_log_timestamp() {
	// get current uptime timestamp
	global $log;
	$timestamp = file_get_contents( $log );

	// check if we have a timestamp
	if ( '' != $timestamp && is_numeric( $timestamp ) ) {
		// check to see if it is longer than 30 seconds ago
		$diff = abs( time() - $timestamp );
		if ( $diff >= 30 ) {
			restart_site();
		}
	}
}

/**
 * Restarts the Local site using LBF
 *
 * @return void
 */
function restart_site() {
	global $log;
	global $lbf;

	// clear log
	file_put_contents( $log, '' );

	// restart the site
	$pretty_time = date( 'Y/m/d h:i:s a', time() );
	echo "-------RESTART AT $pretty_time------\n";
	echo shell_exec( $lbf . ' restart' );
}