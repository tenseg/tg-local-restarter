<?php
/**
 * @package TG Local Restarter
 */
/*
Plugin Name: TG Local Reestarter
Plugin URI: https://bitbucket.org/tenseg/tg-local-restarter/overview
Description: A plugin that helps to restart a Local site as needed when it crashes.
Version: 0.0.1
Author: Tenseg LLC
Author URI: https://www.tenseg.net
License: GPL2
Text Domain: tg-local-restarter

Copyright 2021 Tenseg LLC (dev@tenseg.net)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

// include our config file
// must be in plugin so can be used by external task script
include 'config.php';

class TG_Local_Restarter {
	/**
	 * Class constructor
	 */
	public function __construct() {
		add_action( 'init', [$this, 'cron_init'] );
		add_action( 'tg_local_restarter_cron', [$this, 'log_uptime'] );
		add_action( 'admin_notices', [$this, 'admin_notices'] );
		add_filter( 'cron_schedules', [$this, 'schedules'] );
		register_deactivation_hook( __FILE__, [$this, 'deactivate'] );
	}

	/**
	 * Warn the admin if we can't find our log path setting.
	 *
	 * Called by action: admin_notices
	 *
	 * @return void
	 */
	public function admin_notices() {
		if ( !defined( 'TG_LOCAL_RESTARTER_LOG' ) ) {
			add_settings_error( 'tg_settings_warning', 'tg_cron_dep', __( 'Please add a define for TG_LOCAL_RESTARTER_LOG to be where to log server uptime.', 'tg-local-restarter' ), 'error' );
			settings_errors( 'tg_settings_warning' );
		}
	}

	/**
	 * Add our custom intervals.
	 *
	 * @param array $schedules cron schedules
	 * @return array cron schedules with ours added
	 */
	public static function schedules( $schedules ) {
		$schedules['ten_seconds'] = [
			'interval' => 10,
			'display'  => esc_html__( 'Every 10 Seconds' ),
		];

		return $schedules;
	}

	/**
	 * Initilize the cron job if settings are found
	 *
	 * Called by action: init
	 */
	public function cron_init() {
		// check for settting
		if ( defined( 'TG_LOCAL_RESTARTER_LOG' ) ) {
			// enable cron job
			if ( !wp_next_scheduled( 'tg_local_restarter_cron' ) ) {
				wp_schedule_event( time(), 'ten_seconds', 'tg_local_restarter_cron' );
			}
		} else {
			// unschedule our events if it is no longer desired
			if ( $timestamp = wp_next_scheduled( 'tg_local_restarter_cron' ) ) {
				wp_unschedule_event( $timestamp, 'tg_local_restarter_cron' );
			}
		}
	}

	/**
	 * This runs via cron to log that the site is up.
	 *
	 * @return void
	 */
	public function log_uptime() {
		// get the path to log to
		if ( $path = constant( 'TG_LOCAL_RESTARTER_LOG' ) ) {
			// write the unix time to a file to be checked by our external task
			file_put_contents( $path, time() );
		}
	}

	/**
	 * Make sure cron is off when deactivating TG Local Restarter.
	 *
	 * Called by: register_deactivation_hook
	 *
	 * @return void
	 */
	public static function deactivate() {
		// unschedule our events if it is no longer desired
		if ( $timestamp = wp_next_scheduled( 'tg_local_restarter_cron' ) ) {
			wp_unschedule_event( $timestamp, 'tg_local_restarter_cron' );
		}
	}
}

// Instantiate the class and run the plugin
$tg_local_restarter = new TG_Local_Restarter();