<?php

/*
 * Duplicate this file to `config.php` and fill in the below options. We must use our own file since
 * wp-config.php is not suited to external tasks using it.
 */

/**
 * Turns on and defines log for Local site restart tracker
 */
define( 'TG_LOCAL_RESTARTER_LOG', '/Users/YOU/LocalSites/SITE/logs/uptime.log' );

/**
 * Path to the `lbf` command line tool
 */
define('TG_LOCAL_RESTARTER_LBF_PATH', '/path/to/lbf');