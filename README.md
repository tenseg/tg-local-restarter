# TG Local Restarter

* Version: 0.0.1
* Author: Tenseg LLC
* Author URI: http://www.tenseg.net

A plugin that helps to restart a [Local](https://localwp.com) site as needed when it crashes.

## Requirements

* [lbf](https://bitbucket.org/alexclst/lbf-container/src/main/)

## Setup

1. Clone this plugin into your `wp-content/plugins/` directory.
2. Activate the plugin.
3. Duplicate `config-sample.php` to `config.php` and edit it to your needs.
4. Set up `task.php` as a task in your IDE, see VS Code sample for dettails.